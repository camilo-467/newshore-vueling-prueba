import { ComponentFixture, TestBed } from '@angular/core/testing';
import { GeneralStatisticsTableComponent } from './general-statistics-table.component';

describe('GeneralStatisticsTableComponent', () => {
  let component: GeneralStatisticsTableComponent;
  let fixture: ComponentFixture<GeneralStatisticsTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GeneralStatisticsTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralStatisticsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
