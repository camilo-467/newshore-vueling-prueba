import { Component, OnInit } from '@angular/core';
import { PlayerServicesService } from 'src/app/service/player-services.service';
import { AllPlayers } from 'src/typings';

@Component({
  selector: 'general-statistics-table',
  templateUrl: './general-statistics-table.component.html',
  styleUrls: ['./general-statistics-table.component.scss']
})
export class GeneralStatisticsTableComponent implements OnInit {
  public allPlayers = {} as AllPlayers;
  public headElements = ['Name', 'Surname', 'city', 'Equipment'];

  constructor(private playerServicesService:PlayerServicesService) {}

  ngOnInit(): void {
    this.playerServicesService.GetAllPlayersNBA().subscribe(response=>{
      this.allPlayers = response as AllPlayers;
    });
  }
}
