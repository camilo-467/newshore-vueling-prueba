import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GeneralStatisticsTableComponent } from './general-statistics-table/general-statistics-table.component';
import { PlayerInformationComponent } from './player-information/player-information.component';

const routes: Routes = [
  {
    // this is for when the url is clear
    path:'', pathMatch:'full', redirectTo:'general-statistics' 
  },
  {
    //this is for show table with all player
    path:'general-statistics', component: GeneralStatisticsTableComponent 
  },
  {
    //this is for show single player
    path:'player-information/:id', component: PlayerInformationComponent 
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
