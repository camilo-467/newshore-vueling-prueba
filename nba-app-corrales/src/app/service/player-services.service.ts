import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API } from 'src/environments/environment';
import {catchError} from 'rxjs/internal/operators';
import {throwError} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PlayerServicesService {

  APISinglePlayer: string = API.singlePlayer;
  APIAllPlayer: string = API.allPlayers;

  constructor(private clientHttp:HttpClient) { }

  GetSinglePlayerNBA(idPlayer:string){
    return this.clientHttp.get(this.APISinglePlayer+idPlayer).pipe(catchError( error => { return throwError(error)}));
  }

  GetAllPlayersNBA(){
    return this.clientHttp.get(this.APIAllPlayer).pipe(catchError( error => { return throwError(error)}));
  }
}
