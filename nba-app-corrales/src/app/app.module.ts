import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GeneralStatisticsTableComponent } from './general-statistics-table/general-statistics-table.component';
import { PlayerInformationComponent } from './player-information/player-information.component';

@NgModule({
  declarations: [
    AppComponent,
    GeneralStatisticsTableComponent,
    PlayerInformationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
