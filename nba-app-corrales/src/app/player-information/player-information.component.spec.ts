import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PlayerInformationComponent } from './player-information.component';

describe('PlayerInformationComponent', () => {
  let component: PlayerInformationComponent;
  let fixture: ComponentFixture<PlayerInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlayerInformationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('testing the formatDate method', () => {
    const yearMock = 2021;
    const monthMock = 11;
    const dayMock = 2;
    const dateMock = new Date(yearMock,monthMock,dayMock);
    const result = component.formatDate(dateMock);
    const returnMock = monthMock+' '+dayMock+', '+yearMock;
    expect(result).toEqual(returnMock);
  });
});
