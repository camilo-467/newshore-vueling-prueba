import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Player } from 'src/typings';
import { PlayerServicesService } from '../service/player-services.service';

@Component({
  selector: 'player-information',
  templateUrl: './player-information.component.html',
  styleUrls: ['./player-information.component.scss']
})
export class PlayerInformationComponent implements OnInit {
  private idPlayer:string = '';
  public playerInfo = {} as Player;

  constructor(private activeRoute:ActivatedRoute, private playerServicesService:PlayerServicesService) {}

  ngOnInit(): void {
    this.idPlayer = this.getParameterId();
    this.getPlayerInfo();
  }

  getParameterId():string {
    return this.activeRoute.snapshot.paramMap.get('id') as string;
  }

  getPlayerInfo():void {
    this.playerServicesService.GetSinglePlayerNBA(this.idPlayer).subscribe(response=>{
      this.playerInfo = response as Player;
    });
  }

  // this method receives a date as a parameter and returns the following format 'MM DD, yyyy'
  formatDate(date:Date):string{
    // If the parameter was a string with only one line, this would be resolved ej:
    // const dateBirth = new Date(date).toISOString();
    // toISOString() is for time zone difference
    const day = date.getDay();
    const month = date.toLocaleString('en-US', { month: 'short' });
    const year = date.getFullYear();
    return month+' '+day+', '+year;
  }
}
