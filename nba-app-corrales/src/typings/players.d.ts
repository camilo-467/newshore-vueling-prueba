export interface AllPlayers {
  data: Player[],
  meta: Meta
}

export interface Player {
  first_name: string;
  height_feet?: number;
  height_inches?: number;
  weight_pounds?: number;
  id: number;
  last_name: string;
  position: string;
  team: Team;
}

export interface Team {
  abbreviation: string;
  city: string;
  conference: string;
  division: string;
  full_name: string;
  id: number;
  name: string;
}

export interface Meta {
  current_page: number;
  next_page: number;
  per_page: number;
  total_count: number;
  total_pages: number;
}
